@extends('layouts.template')

@section('title')
<title>Home</title>
@endsection

@section('content')
<div class="row hero-content">
    <div class="col-md-12">
        <h1 class="animated fadeInDown">The Fastest way to mock your API</h1>
        <a href="{{ url('/register') }}" class="use-btn animated fadeInUp">Register</a> 
        <a href="{{ url('/login') }}" class="learn-btn animated fadeInUp">Login</a>
    </div>
</div>
@endsection
