@extends('layouts.template_user')

@section('content')

<div class="body-wrapper">
	<nav id="menu" class="menu">
	    <ul id="tiny">
	        <li class="active"><a href="{{ url('/home') }}"><i class="fa fa-cubes" aria-hidden="true"></i> Project</a>
	        </li>
	        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hideen="true"></i> Logout</a>
	        </li>    
	    </ul>
	</nav>
	  <!-- /.menu -->
	  
	  <div class="box box-border pull-left">
	    <div class="light-wrapper">
	      	<div class="container inner">
		      	<div class="row login">
			        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 well">
			            <form role="form" action="{{route('project.store')}}" method="post" enctype="multipart/form-data">
			            {{ csrf_field() }}
				             <div class="form-group text-center">
				                <div class="logo">
				                    <span><img src="http://cdn.dev.classmethod.jp/wp-content/uploads/2014/05/aws_icon_vpc.png" style="height: 200px;width: 200px"></span>
				                </div>
				            </div>
				            <div class="form-group">
				                <input type="text" name="name" class="form-control input-lg" id="projectname" placeholder="Project name">
				                {!! $errors->first('name','<p class="help-block">:message</p>') !!}
				            </div>
				            <div class="form-group">
				                <input type="text" name="client" class="form-control input-lg" id="clientname" placeholder="Client">
				                {!! $errors->first('client','<p class="help-block">:message</p>') !!}
				            </div>
				            <div>
				                <input type="text" name="user" class="form-control input-lg" placeholder="Username">
				                {!! $errors->first('user','<p class="help-block">:message</p>') !!}
				            </div>
				            <div class="form-group" style="margin-bottom: 15px">
				                <button type="submit" class="btn btn-default btn-lg btn-block btn-success">Save</button>
				            </div>
				            <div class="form-group">
				                <a class="btn btn-default btn-lg btn-block btn-success" href="{{ url('/home') }}">Cancel</a>
				            </div>
			            </form>
			        </div>
		    	</div>
			</div>
		</div>
	</div>
</div>
@endsection