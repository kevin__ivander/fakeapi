@extends('layouts.app')

@section('content')
<nav id="menu" class="menu">
    <ul id="tiny">
        <li class="active"><a href="{{ url('/home') }}"><i class="fa fa-cubes" aria-hidden="true"></i> Project</a>
        </li>
        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hideen="true"></i> Logout</a>
        </li>    
    </ul>
</nav>

<div class="box box-border pull-left">
    <div class="light-wrapper">
        <div class="container inner">
            <div class="row">
                <h2 class="text-center" style="text-align: left !important;
                padding-left: 50px;
                padding-bottom: 20px;
                margin-top: -30px;">Project</h2>
                <div class="col-md-9"></div>
                <a class="btn btn-success" style="margin-bottom: 20px" href="{{route('project.create')}}">Add Project</a>
            </div>

            <div class="row">
                <div class="col-md-10" style="margin-left: 32px">
                    <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Client</th>
                                <th>Username</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>See The Path</th>
                            </tr>
                        </thead>

                        <tfoot>
                            <tr>
                                <th>Project Name</th>
                                <th>Client</th>
                                <th>Username</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>See The Path</th>
                            </tr>
                        </tfoot>

                        <tbody>
                        @foreach($datas as $data)
                            <tr>
                                <td>{{$data->name}}</td>
                                <td>{{$data->client}}</td>
                                <td>{{$data->user}}</td>
                                <td>
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <a class="btn btn-primary btn-xs" data-title="Edit" href="{{route('project.edit',$data->id)}}"><span class="glyphicon glyphicon-pencil"></span></a>
                                    </p>
                                </td>
                                <td>
                                    <form class="" action="{{route('project.destroy',$data->id)}}" method="post">
                                        <a class="btn btn-danger btn-xs" data-title="Delete" type="submit" onclick="return confirm('are you sure to delete this data??');" value="delete"><span class="glyphicon glyphicon-trash"></span></a>
                                    </form>
                                </td>
                                <td>
                                    <p data-placement="top" data-toggle="tooltip" title="Edit">
                                        <a href="{{route('project.show',$data->id)}}"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit"><i class="fa fa-eye" aria-hidden="true"></i>
                                        </button></a>
                                    </p>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection