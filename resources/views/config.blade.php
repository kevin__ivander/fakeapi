<!DOCTYPE html>
<html>
<body>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.o">

<title>INPUT URL
</title>




    <link href="css/input.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>

<div class="container">
    
    <div class="boxbg">
        <h2><strong>Input your URL</strong></h2>
        <hr/>
        <form class="form-group" action="{{url('storeurl')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        
            <div id="divuser" class="form-group has-feedback{{ ($errors->has('url')) ? $errors->first('url') : '' }}">

                <label for="url">URL :</label>

                <div class="input-group">

                    <span class="input-group-addon"><span class="glyphicon glyphicon-send"></span></span>
                    <input type="text" class="form-control" name="url" placeholder="Masukkan URL disini">
                    {!! $errors->first('url','<p class="help-block">:message</p>') !!}
                </div>

                <span id="useroksts" class="glyphicon glyphicon-ok  form-control-feedback" aria-hidden="true"></span>
                <span id="usererrsts" class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>

            </div>   
            
            
            
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="save">
            </div>
   
 
        </form>
    </div>
</div>

</body>
</html>

