@extends('layouts.app')

@section('content')
<aside class="fh5co-page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fh5co-page-heading-lead">
                            Endpoint : {{$tampilkan->endpoint }}
                            <span class="fh5co-border"></span>
                        </h1>
                    </div>
                </div>
            </div>
        </aside>
<div class="col-md-8 col-md-push-2">   
    <div class="row">
        <div class="col-md-8">  
            <h3>Method: {{ $tampilkan->method }}</h3>
            <h3>Deskripsi: {{ $tampilkan->description }}</h3>
            
            <h3>Result Ok: </br><pre id="json">{{ $tampilkan->result_ok }}</pre></h3>
            
            <h3>Result Fail: </br><pre id="json">{{ $tampilkan->result_fail }}</pre></h3>
        </div>
    </div>
    <div class="text-center" style="margin-bottom: 25px">
        </br><a class="btn btn-primary" href="{{url('path/'.$tampilkan->id_project)}}">Ok</a>
    </div>
</div>

@endsection