@extends('layouts.template_user')

@section('content')

<div class="body-wrapper">
	<nav id="menu" class="menu">
	    <ul id="tiny">
	        <li><a href="{{ url('/home') }}"><i class="fa fa-cubes" aria-hidden="true"></i> Project</a>
	        </li>
	        <li class="active"><a href="{{url('path/'.$path->id_project)}}"><i class="fa fa-cube" aria-hidden="true"></i> Path</a>
	        </li>
	        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out" aria-hideen="true"></i> Logout</a>
	        </li>    
	    </ul>
	</nav>

	<div class="box box-border pull-left">
	    <div class="light-wrapper">
	      	<div class="container inner">
		      	<div class="row login">
			        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 well">
			            <form role="form" action="{{url('updatepath', $path->id)}}" method="POST" enctype="multipart/form-data">
			            {{ csrf_field() }}
				             <div class="form-group text-center">
				                <div class="logo">
				                    <span><img src="http://cdn.dev.classmethod.jp/wp-content/uploads/2014/05/aws_icon_vpc.png" style="height: 200px;width: 200px"></span>
				                </div>
				             </div>
				             <div class="form-group">
				             	<label for="endpoint">End Point</label>
				                <input type="text" class="form-control input-lg" id="endpoint" placeholder="End Point" value="{{ $path->endpoint }}" required="" name="endpoint">
				                {!! $errors->first('endpoint','<p class="help-block">:message</p>') !!}
				             </div>
			              	<div class="form-group ">
			              		<label for="method">Method</label>
	                            <div class="col-lg-10 form-control">
	                                @if($path->method=='GET')
	                                <select class="form-control m-bot15" id="method" name="method">
	                                    <option value="GET" selected>GET</option>
	                                    <option value="POST">POST</option>
	                                    <option value="PATCH">PATCH</option>
	                                    <option value="DELETE">DELETE</option>
	                                </select>
	                                @elseif($path->method=='POST')
	                                <select class="form-control m-bot15" id="method" name="method">
	                                    <option value="GET">GET</option>
	                                    <option value="POST" selected>POST</option>
	                                    <option value="PATCH">PATCH</option>
	                                    <option value="DELETE">DELETE</option>
	                                </select>
	                                @elseif($path->method=='PATCH')
	                                <select class="form-control m-bot15" id="method" name="method">
	                                    <option value="GET">GET</option>
	                                    <option value="POST">POST</option>
	                                    <option value="PATCH" selected>PATCH</option>
	                                    <option value="DELETE">DELETE</option>
	                                </select>
	                                @elseif($path->method=='DELETE')
	                                <select class="form-control m-bot15" id="method" name="method">
	                                    <option value="GET">GET</option>
	                                    <option value="POST">POST</option>
	                                    <option value="PATCH">PATCH</option>
	                                    <option value="DELETE" selected>DELETE</option>
	                                </select>
	                                @endif
	                            </div>
	                        </div>
				            <div>
				            	<label for="Description">Description</label>
				                <input type="text" name="description" id="description" class="form-control input-lg" placeholder="Description" value="{{ $path->description }}" required="">
				                {!! $errors->first('description','<p class="help-block">:message</p>') !!}
				            </div>
				            <div class="form-group" style="margin-bottom: 15px">
				                <label for="myEditorOk">Result OK {JSON}</label>
				                <div class="editor" id="myEditorResultOK" style="height: 250px" ></div>
				                <textarea id="result_ok" name="result_ok">{{$path->result_ok}}</textarea>
				                {!! $errors->first('myEditorOk','<p class="help-block">:message</p>') !!}
				            </div>

				            <div class="form-group" style="margin-bottom: 15px">
				                <label for="myEditorFail">Result Fail {JSON}</label>
				                <div class="editor" id="myEditorResultFail" name="result_fail" style="height: 250px"></div>
								  	<textarea id="result_fail" name="result_fail">{{$path->result_fail}}</textarea>
				                {!! $errors->first('myEditorFail','<p class="help-block">:message</p>') !!}
				            </div>
				            <div class="form-group" style="margin-bottom: 15px">
				                <button type="submit" class="btn btn-default btn-lg btn-block btn-success">Update</button>
				            </div>
				            <div class="form-group">
				                <a href="{{url('path/'.$path->id_project)}}" class="btn btn-default btn-lg btn-block btn-success">Cancel</a>
				            </div>
			            </form>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>
<script src="http://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js"></script>
<script>
  $('textarea').asAceEditor();
</script>

<script type="text/javascript">
	var editors = document.querySelectorAll(".editor");
	Array.prototype.forEach.call(editors, function (el) {
	  	var editor = ace.edit(el);
	  	
	  	editor.setTheme("ace/theme/tomorrow");
	  
	  	if (el.id === "myEditorResultOK") {
	    	editor.getSession().setMode("ace/mode/json");
	    	var textarea = $('textarea[name="result_ok"]').hide();
			editor.getSession().setValue(textarea.val());
			editor.getSession().on('change', function(){
			  	textarea.val(editor.getSession().getValue());
			});
			
	  	}
	  	if (el.id === "myEditorResultFail") {
	    	editor.getSession().setMode("ace/mode/json");
	    	var textarea = $('textarea[name="result_fail"]').hide();
			editor.getSession().setValue(textarea.val());
			editor.getSession().on('change', function(){
			  	textarea.val(editor.getSession().getValue());
			});
			editor.setValue('{\n\t"status": false,\n\t"status_code": 201,\n\t"message": "data not found"\n}');
	  	}
	});
</script>

@endsection