<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Fake API</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('style/images/favicon.png')}}" />
<link href="{{asset('style/css/bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('style/css/settings.css')}}" rel="stylesheet">
<link href="{{asset('style/js/google-code-prettify/prettify.css')}}" rel="stylesheet">
<link href="{{asset('style/js/fancybox/jquery.fancybox.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset('style/js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.2')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/style.css')}}" rel="stylesheet">
<link href="{{asset('style/css/color/green.css')}}" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<link href="{{asset('style/type/fontello.css')}}" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

</head>
<body>
    <header>
        <div class="logo pull-left">
            <a href="index.html"><img src="{{asset('style/images/logo.png')}}" alt="" style="height: 20px;width: 100px" /></a>
        </div>
        <ul class="contact-info pull-right">
            <li><i class="icon-mail"></i><a href="mailto:">your_email@mail.co.id</a></li>
        </ul>
    </header>

    @yield('content')

    <script src="{{asset('style/js/jquery.js')}}"></script> 
    <script src="{{asset('style/js/bootstrap.min.js')}}"></script> 
    <script src="{{asset('style/js/twitter-bootstrap-hover-dropdown.min.js')}}"></script> 
    <script src="{{asset('style/js/ddsmoothmenu.js')}}"></script> 
    <script src="{{asset('style/js/jquery.themepunch.plugins.min.js')}}"></script> 
    <script src="{{asset('style/js/jquery.themepunch.revolution.min.js')}}"></script> 
    <script src="{{asset('style/js/jquery.themepunch.showbizpro.min.js')}}"></script> 
    <script src="{{asset('style/js/jquery.fancybox.pack.js')}}"></script> 
    <script src="{{asset('style/js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.2')}}"></script> 
    <script src="{{asset('style/js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.0')}}"></script> 
    <script src="{{asset('style/js/jquery.meanmenu.2.0.min.js')}}"></script> 
    <script src="{{asset('style/js/jquery.fitvids.js')}}"></script> 
    <script src="{{asset('style/js/jquery.slickforms.js')}}"></script> 
    <script src="{{asset('style/js/jquery.isotope.min.js')}}"></script> 
    <script src="{{asset('style/js/google-code-prettify/prettify.js')}}"></script> 
    <script src="{{asset('style/js/jquery.easytabs.min.js')}}"></script> 
    <script src="{{asset('style/js/jquery.hoverdir.min.js')}}"></script> 
    <script src="{{asset('style/js/scripts.js')}}"></script>
    <script type="text/javascript">

        var urlBase = "<?php echo Request::root(); ?>"

        $(function(){

              function requestData1() {
                $.ajax({
                    url: urlBase + '/session/ajaxData1',
                    success: function(point) {

                        setTimeout(requestData1, 500);    
                    },
                    cache: false
                });
              }

              requestData1();

        });
    </script>
    
    <!-- DEMO ONLY -->
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/blue.css')}}" title="seabirdblue-color" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/green.css')}}" title="seabirdgreen-color" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/orange.css')}}" title="seabirdorange-color" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/pink.css')}}" title="seabirdpink-color" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/purple.css')}}" title="seabirdpurple-color" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/rose.css')}}" title="seabirdrose-color" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg1.css')}}" title="seabirdbg1-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg2.css')}}" title="seabirdbg2-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg3.css')}}" title="seabirdbg3-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg4.css')}}" title="seabirdbg4-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg5.css')}}" title="seabirdbg5-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg6.css')}}" title="seabirdbg6-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg7.css')}}" title="seabirdbg7-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg8.css')}}" title="seabirdbg8-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg9.css')}}" title="seabirdbg9-bg" media="screen" />
    <link rel="alternate stylesheet" type="text/css" href="{{asset('switcher/bg10.css')}}" title="seabirdbg10-bg" media="screen" />
    <script type="text/javascript" src="{{asset('switcher/switchstylesheet.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".changecolor").switchstylesheet({
                seperator: "color"
            });
            $(".changebg").switchstylesheet({
                seperator: "bg"
            });
        });
    </script>
    
</body>
</html>