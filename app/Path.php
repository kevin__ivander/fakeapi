<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Path extends Model
{
    protected $table = 'path';
    protected $primaryKey = 'id';
    protected $fillable = ['endpoint',
    						'method',
    						'description',
    						'id_project'];
    public $timestamps = false;
}
