<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Path;

use Auth;

class PathController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware('auth');        
    // }

    public function index($id)
    {
        $datas = Path::where('id_project', $id)->orderBy('id','DESC')->paginate(10);
        return view('paths.path')->with('datas',$datas);
    }
    public function index1($id)
    {
        $datas = Path::where('id_project', $id)->orderBy('id','DESC')->paginate(10);
        return view('paths.path',compact('id',$id))->with('datas',$datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //$data['setID'] = $id;
        return view('paths.addpath')->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //var_dump($request->result_ok);die();
        $this->validate($request,[
        'endpoint'=>'required',
        'method'=>'required',
        'description'=>'required',
        'result_ok'=>'required', 
        'result_fail'=>'required', 
        ]);


        $path = new path;
        //$path->id_project= $request->projectID;
        $path->id_project=$id;
        $path->endpoint=$request->endpoint;
        $path->method=$request->method;
        $path->description=$request->description;
        $path->result_ok=$request->result_ok;
        $path->result_fail=$request->result_fail;
        $path->save();
        
        return redirect()->to('/path/'.$id)->with('alert-success','Data Has been Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tampilkan = Path::find($id);
        return view('paths.showpath',compact('id',$id))->with('tampilkan', $tampilkan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $path = Path::find($id);
        return view('paths.editpath')->with('path', $path);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
        'endpoint'=>'required',
        'method'=>'required',
        'description'=>'required',
        'result_ok'=>'required', 
        'result_fail'=>'required', 
        ]);

        $path = Path::where('id', $id)->first();
        $path->endpoint=$request->endpoint;
        $path->method=$request->method;
        $path->description=$request->description;
        $path->result_ok=$request->result_ok;
        $path->result_fail=$request->result_fail;
        $path->save();

        $cari = Path::find($id);
        $idproject = $cari->id_project;
        
        return redirect()->to('/path/'.$idproject)->with('alert-success','Data Has been Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

        $hapus = Path::find($id);
        $idproject = $hapus->id_project;
        $hapus->delete();

        return redirect()->to('/path/'.$idproject)->with('alert-success','Data Has been Deleted!');
    }
}
