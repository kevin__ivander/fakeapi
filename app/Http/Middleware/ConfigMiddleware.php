<?php

namespace App\Http\Middleware;

use App\Config;

use Closure;

class ConfigMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        
        $collection = Config::All();
        
        if(count($collection)==0){
            return redirect('instal');
        }
        
        return $next($request);
    }
}
