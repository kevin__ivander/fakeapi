<?php

Route::get('instal', 'ConfigController@index');

Route::get('/', function () {
    return view('welcome');
});
Route::get('/homes', function () {
    return view('home');
});

Route::post('/storeurl', 'ConfigController@store');

Route::group(['middleware' => 'config'], function () {
	
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::get('/homes', function () {
	    return view('home');
	});

	Route::get('/home', 'ProjectController@index');

	Route::auth();

	Route::resource('/project', 'ProjectController');

	Route::get('/path/{id}', 'PathController@index1');

	Route::get('/path/addpath/{id}', 'PathController@create');

	Route::post('/storepath/{id}', 'PathController@store');

	Route::get('/showpath/{id}', 'PathController@show');

	Route::get('/editpath/{id}', 'PathController@edit');

	Route::post('/updatepath/{id}', 'PathController@update');

	Route::delete('/destroypath/{id}', 'PathController@destroy');
});

Route::post('/registerapi', 'ApiController@registerapi');

Route::post('/loginapi', 'ApiController@loginapi');

Route::group(['middleware' => 'jwt-auth'], function () {

	Route::post('/get_user_details1', 'ApiController@get_user_details');
	
	Route::get('/{project?}/{endpoint?}','ApiController@makeApi');
});